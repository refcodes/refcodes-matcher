# README #

> The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.

## What is this repository for? ##

***This artifact allows the construction of matchers for building arbitrary complex matching functionality. It is used to test whether matchee instances match certain criteria. [`Matcher`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/latest/org.refcodes.matcher/org/refcodes/matcher/Matcher.html) instances may get nested in a hierarchy (using e.g. the [`OrMatcher`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/latest/org.refcodes.matcher/org/refcodes/matcher/OrMatcher.html), the [`AndMatcher`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/latest/org.refcodes.matcher/org/refcodes/matcher/AndMatcher.html) or the [`NotMatcher`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/latest/org.refcodes.matcher/org/refcodes/matcher/NotMatcher.html) types), having different matchers in combination will do custom matching. Some examples for [`Matcher`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/latest/org.refcodes.matcher/org/refcodes/matcher/Matcher.html) implementations are the [`EqualWithMatcher`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/latest/org.refcodes.matcher/org/refcodes/matcher/EqualWaithMatcher.html), the [`PathMatcher`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/latest/org.refcodes.matcher/org/refcodes/matcher/PathMatcher.html),the [`LessThanMatcher`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/latest/org.refcodes.matcher/org/refcodes/matcher/LessThanMatcher.html) or the [`RegExpMatcher`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/latest/org.refcodes.matcher/org/refcodes/matcher/RegExpMatcher.html). Use the [`MatcherSugar`](https://www.javadoc.io/doc/org.refcodes/refcodes-matcher/latest/org.refcodes.matcher/org/refcodes/matcher/MatcherSugar.html) syntactic sugar mixin for expressively building your matchers.***

## Getting started ##

> Please refer to the [refcodes-matcher: Build custom matching logic and match path patterns](https://www.metacodes.pro/refcodes/refcodes-matcher) documentation for an up-to-date and detailed description on the usage of this artifact. 

## How do I get set up? ##

To get up and running, include the following dependency (without the three dots "...") in your `pom.xml`:

```
<dependencies>
	...
	<dependency>
		<groupId>org.refcodes</groupId>
		<artifactId>refcodes-matcher</artifactId>
		<version>3.3.9</version>
	</dependency>
	...
</dependencies>
```

The artifact is hosted directly at [Maven Central](http://search.maven.org). Jump straight to the source codes at [Bitbucket](https://bitbucket.org/refcodes/refcodes-matcher). Read the artifact's javadoc at [javadoc.io](http://www.javadoc.io/doc/org.refcodes/refcodes-matcher).

## Contribution guidelines ##

* [Report issues](https://bitbucket.org/refcodes/refcodes-matcher/issues)
* Finding bugs
* Helping fixing bugs
* Making code and documentation better
* Enhance the code

## Who do I talk to? ##

* Siegfried Steiner (steiner@refcodes.org)

## Terms and conditions ##

The [`REFCODES.ORG`](http://www.refcodes.org/refcodes) group of artifacts is published under some open source licenses; covered by the  [`refcodes-licensing`](https://bitbucket.org/refcodes/refcodes-licensing) ([`org.refcodes`](https://bitbucket.org/refcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.