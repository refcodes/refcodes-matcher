module org.refcodes.matcher {
	requires transitive org.refcodes.mixin;
	requires transitive org.refcodes.schema;
	requires org.refcodes.data;

	exports org.refcodes.matcher;
}
