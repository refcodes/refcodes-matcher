// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.matcher;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.refcodes.mixin.Dumpable;
import org.refcodes.mixin.WildcardSubstitutes;

/**
 * The {@link AbstractWildcardMatcher} provides a base functionality for
 * {@link Matcher} implementations.
 * 
 * @param <M> The matchee type
 */
public abstract class AbstractWildcardMatcher<M> extends AbstractMatcher<M> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ALIAS = "WILDCARD";
	protected static final String REGEX_PATTERN = "REGEX_PATTERN";
	protected static final String WILDCARD_NAMES = "WILDCARD_NAMES";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link AbstractWildcardMatcher} with the given description.
	 * 
	 * @param aDescription The according matcher's description.
	 */
	public AbstractWildcardMatcher( String aDescription ) {
		super( ALIAS, aDescription );
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * The Class WildcardMatcherSubstitutes.
	 */
	protected static class WildcardMatcherSubstitutes implements WildcardSubstitutes, Dumpable {

		// /////////////////////////////////////////////////////////////////////////
		// STATICS:
		// /////////////////////////////////////////////////////////////////////////

		// /////////////////////////////////////////////////////////////////////////
		// CONSTANTS:
		// /////////////////////////////////////////////////////////////////////////

		// /////////////////////////////////////////////////////////////////////////
		// VARIABLES:
		// /////////////////////////////////////////////////////////////////////////

		private final String[] _wildcardSubstitutes;
		private final Map<String, String> _namedWildcardSubstittutes;

		// /////////////////////////////////////////////////////////////////////////
		// CONSTRUCTORS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * Instantiates a new wildcard substitutes impl.
		 *
		 * @param aWildcardSubstitutes the wildcard substitutes
		 * @param aNamedWildcardSubstittutes the named wildcard substittutes
		 */
		public WildcardMatcherSubstitutes( String[] aWildcardSubstitutes, Map<String, String> aNamedWildcardSubstittutes ) {
			_wildcardSubstitutes = aWildcardSubstitutes;
			_namedWildcardSubstittutes = aNamedWildcardSubstittutes;
		}

		// /////////////////////////////////////////////////////////////////////////
		// METHODS:
		// /////////////////////////////////////////////////////////////////////////

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String[] getWildcardReplacements() {
			return _wildcardSubstitutes;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getWildcardReplacementAt( int aIndex ) {
			if ( _wildcardSubstitutes != null ) {
				if ( _wildcardSubstitutes.length > aIndex ) {
					return _wildcardSubstitutes[aIndex];
				}
			}
			return null;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String getWildcardReplacement( String aWildcardName ) {
			if ( _namedWildcardSubstittutes != null ) {
				return _namedWildcardSubstittutes.get( aWildcardName );
			}
			return null;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String[] getWildcardReplacements( String... aWildcardNames ) {
			final List<String> theWildcardSubstitutes = new ArrayList<>();
			String eSubstitute;
			for ( String eName : aWildcardNames ) {
				eSubstitute = getWildcardReplacement( eName );
				if ( eSubstitute == null ) {
					throw new IllegalArgumentException( "There is none such named wildcard substitute for name <" + eName + ">, check your pattern!." );
				}
				theWildcardSubstitutes.add( eSubstitute );
			}
			return theWildcardSubstitutes.toArray( new String[theWildcardSubstitutes.size()] );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String[] getWildcardReplacementsAt( int... aIndexes ) {
			final List<String> theWildcardSubstitutes = new ArrayList<>();
			for ( int i : aIndexes ) {
				if ( i >= _wildcardSubstitutes.length ) {
					throw new IllegalArgumentException( "Your provided index <" + i + "> exceeds the number of wildcards <" + _wildcardSubstitutes.length + "> defined in your pattern." );
				}
				theWildcardSubstitutes.add( _wildcardSubstitutes[i] );
			}
			return theWildcardSubstitutes.toArray( new String[theWildcardSubstitutes.size()] );
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String[] getWildcardNames() {
			if ( _namedWildcardSubstittutes != null && _namedWildcardSubstittutes.size() != 0 ) {
				return _namedWildcardSubstittutes.keySet().toArray( new String[_namedWildcardSubstittutes.size()] );
			}
			return null;
		}

		/**
		 * {@inheritDoc}
		 */
		@Override
		public String toString() {
			return getClass().getSimpleName() + " [wildcardSubstitutes=" + Arrays.toString( _wildcardSubstitutes ) + ", namedWildcardSubstittutes=" + _namedWildcardSubstittutes + "]";
		}
	}
}
