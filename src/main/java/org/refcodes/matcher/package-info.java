// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

/**
 * This artifact allows the construction of matchers for building arbitrary
 * complex matching functionality. It is used to test whether matchee instances
 * match certain criteria. {@link org.refcodes.matcher.Matcher} instances may
 * get nested in a hierarchy (using e.g. the
 * {@link org.refcodes.matcher.OrMatcher}, the
 * {@link org.refcodes.matcher.AndMatcher} or the
 * {@link org.refcodes.matcher.NotMatcher} types), having different matchers in
 * combination will do custom matching. Some examples for
 * {@link org.refcodes.matcher.Matcher} implementations are the
 * {@link org.refcodes.matcher.EqualWithMatcher}, the
 * {@link org.refcodes.matcher.PathMatcher},the
 * {@link org.refcodes.matcher.LessThanMatcher} or the
 * {@link org.refcodes.matcher.RegExpMatcher}. Use the
 * {@link org.refcodes.matcher.MatcherSugar} syntactic sugar mixin for
 * expressively building your matchers.
 * 
 * <p style="font-style: plain; font-weight: normal; background-color: #f8f8ff;
 * padding: 1.5rem; border-style: solid; border-width: 1pt; border-radius: 10pt;
 * border-color: #cccccc;text-align: center;">
 * Please refer to the <a href=
 * "https://www.metacodes.pro/refcodes/refcodes-matcher"><strong>refcodes-matcher:
 * Build custom matching logic and match path patterns</strong></a>
 * documentation for an up-to-date and detailed description on the usage of this
 * artifact.
 * </p>
 * 
 */
package org.refcodes.matcher;
