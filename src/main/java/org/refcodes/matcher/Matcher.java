// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.matcher;

import org.refcodes.mixin.AliasAccessor;
import org.refcodes.schema.Schema;
import org.refcodes.schema.Schemable;

/**
 * The matcher is used to test whether a matchee matches certain criteria. A
 * matcher may be an own implementation or it may get nested (inspired by
 * http://code.mycila.com/wiki/MycilaEvent) having different matchers which in
 * combination do some custom matching as needed by the business logic. The
 * method {@link #toSchema()} discovers a {@link Schema} from a {@link Matcher}
 * for documenting or debugging purposes of such a nested {@link Matcher}
 * structure,
 * 
 * @param <M> The matchee type.
 */
@FunctionalInterface
public interface Matcher<M extends Object> extends Matchable<M>, Schemable, AliasAccessor {

	/**
	 * {@inheritDoc}
	 */
	@Override
	default MatcherSchema toSchema() {
		return new MatcherSchema( getAlias(), getClass() );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	default String getAlias() {
		return getClass().getName();
	}
}
