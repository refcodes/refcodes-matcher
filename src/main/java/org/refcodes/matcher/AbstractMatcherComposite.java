// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////
package org.refcodes.matcher;

/**
 * The {@link AbstractMatcherComposite} is composed of multiple {@link Matcher}
 * instances queried upon match requests as of {@link #isMatching(Object)}.
 * 
 * @param <M> The matchee type
 */
public abstract class AbstractMatcherComposite<M> extends AbstractMatcher<M> {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	protected Matcher<M>[] _matchers;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an {@link AbstractMatcherComposite} with the given description
	 * and matchers.
	 * 
	 * @param aAlias The alias for the according {@link Matcher} implementation.
	 * @param aDescription The according matcher's description.
	 * @param aMatchers The matchers used when matching.
	 */
	@SuppressWarnings("unchecked")
	public AbstractMatcherComposite( String aAlias, String aDescription, Matcher<M>... aMatchers ) {
		super( aAlias, aDescription );
		assert ( aMatchers != null );
		for ( int i = 0; i < aMatchers.length; i++ ) {
			assert ( aMatchers != null );
		}
		_matchers = aMatchers;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MatcherSchema toSchema() {
		final MatcherSchema theSchema = super.toSchema();
		MatcherSchema[] theSchemas = null;
		if ( _matchers != null && _matchers.length != 0 ) {
			theSchemas = new MatcherSchema[_matchers.length];
			for ( int i = 0; i < theSchemas.length; i++ ) {
				theSchemas[i] = _matchers[i].toSchema();
			}
		}
		return new MatcherSchema( theSchema, theSchemas );
	}
}
