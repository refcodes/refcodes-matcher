// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.matcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.refcodes.data.Delimiter;
import org.refcodes.data.Wildcard;
import org.refcodes.mixin.DelimiterAccessor;
import org.refcodes.mixin.WildcardSubstitutes;

/**
 * The {@link PathMatcher} provides {@link WildcardMatcher} functionality for
 * matching paths: The {@link PathMatcher} matches its ANT like path pattern
 * against the path provided to the {@link #isMatching(String)} and the like
 * methods. The {@link PathMatcher} applies the following rules from the ANT
 * path pattern to the path provided via {@link #isMatching(String)} method: A
 * single asterisk ("*" as of {@link Wildcard#FILE}) matches zero or more
 * characters within a path name. A double asterisk ("**" as of
 * {@link Wildcard#PATH}) matches zero or more characters across directory
 * levels. A question mark ("?" as of {@link Wildcard#CHAR}) matches exactly one
 * character within a path name. The single asterisk ("*" as of
 * {@link Wildcard#FILE}), the double asterisk ("**" as of
 * {@link Wildcard#PATH}) and the question mark ("?" as of
 * {@link Wildcard#CHAR}) we refer to as wildcards: You get an array with the
 * substitutes of the wildcards using the method
 * {@link #toWildcardSubstitutes(String)} (or null, if your {@link String} does
 * not match your path pattern). You may name a wildcard by prefixing it with
 * "${someWildcardName}=". For example a named wildcard may look as follows:
 * "${arg1}=*" or "${arg2}=**" or "${arg3}=?". A placeholder "${arg1}" with no
 * wildcard assignment "=" is equivalent to "${arg1}=*". The regular expression
 * pattern construction is inspired by:
 * "http://stackoverflow.com/questions/33171025/regex-matching-an-ant-path"
 */
public class PathMatcher extends AbstractWildcardMatcher<String> implements WildcardMatcher, DelimiterAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final char OPEN_CURLY = '{'; // ${...}
	private static final char DOLLAR = '$'; // ${...}
	private static final char[] ESCAPE_INSIDE_CHARACTER_CLASS = { '^', '-', ']', '\\' };
	private static final char[] ESCAPE_OUTSIDE_CHARACTER_CLASS = { '.', '^', '$', '*', '+', '?', '(', ')', '[', '{', '\\', '|' };

	private static final String DELIMITER = "DELIMITER";
	private static final String PATH_PATTERN = "PATH_PATTERN";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Pattern _matchee;
	private String _pathPattern = null;
	private String[] _wildcardNames = null;
	private char _delimiter;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an ANT Path {@link Matcher}, matching its ANT path pattern
	 * against the path provided to the {@link #isMatching(String)} method. The
	 * <code>org.refcodes.data.Delimiter#PATH</code> is used as default path
	 * delimiter. The {@link PathMatcher} applies the following rules from the
	 * ANT path pattern to the path provided via {@link #isMatching(String)}
	 * method: A single asterisk ("**" as of {@link Wildcard#FILE}) matches zero
	 * or more characters within a path name. A double asterisk ("**" as of
	 * {@link Wildcard#PATH}) matches zero or more characters across directory
	 * levels. A question mark ("?" as of {@link Wildcard#CHAR}) matches exactly
	 * one character within a path name.
	 * 
	 * @param aPathPattern The pattern to be used when matching a path via
	 *        {@link #isMatching(String)}.
	 */
	public PathMatcher( String aPathPattern ) {
		this( aPathPattern, Delimiter.PATH.getChar() );
	}

	/**
	 * Constructs an ANT Path {@link Matcher}, matching its ANT path pattern
	 * against the path provided to the {@link #isMatching(String)} method. The
	 * <code>org.refcodes.data.Delimiter#PATH</code> is used as default path
	 * delimiter. The {@link PathMatcher} applies the following rules from the
	 * ANT path pattern to the path provided via {@link #isMatching(String)}
	 * method: A single asterisk ("**" as of {@link Wildcard#FILE}) matches zero
	 * or more characters within a path name. A double asterisk ("**" as of
	 * {@link Wildcard#PATH}) matches zero or more characters across directory
	 * levels. A question mark ("?" as of {@link Wildcard#CHAR}) matches exactly
	 * one character within a path name.
	 * 
	 * @param aPathPattern The pattern to be used when matching a path via
	 *        {@link #isMatching(String)}.
	 * @param aDelimiter The delimiter separating the path elements from each
	 *        other and must not be one of the following chars: '?', '*', '|'.
	 * 
	 * @throws IllegalArgumentException thrown in case the delimiter provided
	 *         was one of the following chars: '?', '*', '|'.
	 */
	public PathMatcher( String aPathPattern, char aDelimiter ) {
		super( "Matches a path against an ant path pattern (ANT)." );
		if ( aDelimiter == '?' || aDelimiter == '*' || aDelimiter == '|' ) {
			throw new IllegalArgumentException( "Your delimiter '" + aDelimiter + "' must not be one of the following chars: '?', '*', '|'." );
		}
		_delimiter = aDelimiter;
		_pathPattern = aPathPattern;
		final String theRegex = toRegex( aPathPattern, aDelimiter );
		_matchee = Pattern.compile( theRegex );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getWildcardNames() {
		return _wildcardNames;
	}

	/**
	 * Returns the path pattern being used by the {@link PathMatcher}.
	 * 
	 * @return The path pattern being used.
	 */
	public String getPathPattern() {
		return _pathPattern;
	}

	/**
	 * The this method applies the following rules from the configured ANT path
	 * pattern to the path provided via {@link #isMatching(String)} method: A
	 * single asterisk ("*" as of {@link Wildcard#FILE}) matches zero or more
	 * characters within a path name. A double asterisk ("**" as of
	 * {@link Wildcard#PATH}) matches zero or more characters across directory
	 * levels. A question mark ("?" as of {@link Wildcard#CHAR}) matches exactly
	 * one character within a path name {@inheritDoc}
	 */
	@Override
	public boolean isMatching( String aPath ) {
		return _matchee.matcher( aPath ).matches();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WildcardSubstitutes toWildcardSubstitutes( String aPath ) {
		final java.util.regex.Matcher theMatcher = _matchee.matcher( aPath );
		if ( !theMatcher.matches() ) {
			return null;
		}

		// Unnamed substitutes -->
		final List<String> theWildcardSubstitutes = new ArrayList<>();
		for ( int i = 0; i < theMatcher.groupCount(); i++ ) {
			theWildcardSubstitutes.add( theMatcher.group( i + 1 ) );
		}
		final String[] theWildcardReplacements = theWildcardSubstitutes.toArray( new String[theWildcardSubstitutes.size()] );
		// <-- Unnamed substitutes

		// Named substitutes -->
		Map<String, String> theNamedWildcardSubstitutes = null;
		if ( _wildcardNames != null ) {
			theNamedWildcardSubstitutes = new HashMap<>();
			for ( String eName : _wildcardNames ) {
				theNamedWildcardSubstitutes.put( eName, theMatcher.group( eName ) );
			}
		}
		// <-- Named substitutes

		return new WildcardMatcherSubstitutes( theWildcardReplacements, theNamedWildcardSubstitutes );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toWildcardReplacements( String aPath ) {
		final java.util.regex.Matcher theMatcher = _matchee.matcher( aPath );
		if ( !theMatcher.matches() ) {
			return null;
		}
		final List<String> theWildcardSubstitutes = new ArrayList<>();
		for ( int i = 0; i < theMatcher.groupCount(); i++ ) {
			theWildcardSubstitutes.add( theMatcher.group( i + 1 ) );
		}
		return theWildcardSubstitutes.toArray( new String[theWildcardSubstitutes.size()] );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toWildcardReplacementAt( String aPath, int aIndex ) {
		final java.util.regex.Matcher theMatcher = _matchee.matcher( aPath );
		if ( !theMatcher.matches() ) {
			return null;
		}
		if ( aIndex < theMatcher.groupCount() ) {
			return theMatcher.group( aIndex + 1 );
		}
		throw new IllegalArgumentException( "Your provided index <" + aIndex + "> exceeds the number if wildcards <" + theMatcher.groupCount() + "> defined in your pattern \"" + getPathPattern() + "\" for current path \"" + aPath + "\"." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toWildcardReplacementsAt( String aPath, int... aIndexes ) {
		final java.util.regex.Matcher theMatcher = _matchee.matcher( aPath );
		if ( !theMatcher.matches() ) {
			return null;
		}
		if ( aIndexes == null || aIndexes.length == 0 ) {
			return new String[] {};
		}
		final List<String> theWildcardSubstitutes = new ArrayList<>();
		for ( int i : aIndexes ) {
			if ( i >= theMatcher.groupCount() ) {
				throw new IllegalArgumentException( "Your provided index <" + i + "> exceeds the number of wildcards <" + theMatcher.groupCount() + "> defined in your pattern \"" + getPathPattern() + "\" for current path \"" + aPath + "\"." );
			}
			theWildcardSubstitutes.add( theMatcher.group( i + 1 ) );
		}
		return theWildcardSubstitutes.toArray( new String[theWildcardSubstitutes.size()] );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toWildcardReplacement( String aPath, String aWildcardName ) {
		final java.util.regex.Matcher theMatcher = _matchee.matcher( aPath );
		if ( !theMatcher.matches() ) {
			return null;
		}
		try {
			return theMatcher.group( aWildcardName );
		}
		catch ( IllegalArgumentException e ) {
			throw new IllegalArgumentException( "Your provided name <" + aWildcardName + "> is not defined in your pattern \"" + getPathPattern() + "\" for current path \"" + aPath + "\".", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toWildcardReplacements( String aPath, String... aWildcardNames ) {
		final java.util.regex.Matcher theMatcher = _matchee.matcher( aPath );
		if ( !theMatcher.matches() ) {
			return null;
		}
		if ( aWildcardNames == null || aWildcardNames.length == 0 ) {
			return new String[] {};
		}
		final List<String> theWildcardSubstitutes = new ArrayList<>();
		for ( String eWildcardName : aWildcardNames ) {
			try {
				theWildcardSubstitutes.add( theMatcher.group( eWildcardName ) );
			}
			catch ( IllegalArgumentException e ) {
				throw new IllegalArgumentException( "Your provided name <" + eWildcardName + "> is not defined in your pattern \"" + getPathPattern() + "\" for current path \"" + aPath + "\".", e );
			}
		}
		return theWildcardSubstitutes.toArray( new String[theWildcardSubstitutes.size()] );
	}

	/**
	 * For debugging purposes, retrieve the regex pattern created from the ANT
	 * path pattern.
	 * 
	 * @return Returns the regex created from the ANT path pattern.
	 */
	public String toPattern() {
		return _matchee.pattern();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _matchee == null ) ? 0 : _matchee.hashCode() );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final PathMatcher other = (PathMatcher) obj;
		if ( _matchee == null ) {
			if ( other._matchee != null ) {
				return false;
			}
		}
		else if ( !_matchee.equals( other._matchee ) ) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public char getDelimiter() {
		return _delimiter;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MatcherSchema toSchema() {
		final MatcherSchema theSchema = super.toSchema();
		theSchema.put( REGEX_PATTERN, _matchee );
		theSchema.put( DELIMITER, _delimiter );
		theSchema.put( PATH_PATTERN, _pathPattern );
		theSchema.put( WILDCARD_NAMES, _wildcardNames );
		return theSchema;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private String toRegex( String aPathPattern, char aDelimiter ) {

		aPathPattern = toEscapedReservedChars( aPathPattern );

		// Previously |-->
		// @formatter:off
			/*
				String WILDCARD_NAME_PATTERN = "(?<=\\$\\{).*?(?=\\}\\=)"; // ${name}=
				int WILDCARD_NAME_PREFIX_LENGTH = 2; // "${"
				int WILDCARD_NAME_SUFFIX_LENGTH = 2; // "}="
				String SIMPLE_NAME_PATTERN = "(?<=\\$\\{).*?(?=\\})"; // ${name}
				int SIMPLE_NAME_PREFIX_LENGTH = 2; // "${"
				int SIMPLE_NAME_SUFFIX_LENGTH = 1; // "}"
				String WILDCARD_ASTERISK = "([^\\/]*)";
				String WILDCARD_DOUBOLE_ASTERISK = "(.*)";
				String WILDCARD_QUESTION_MARK = "(\\\\w)";
				String REGEX_SAFE_SLASH = "\\/";
				String REGEX_SAFE_DOT = "\\.";
				// Convert the wildcards into regex pattern groups -->
				String theRegex = aPathPattern;
				theRegex = aPathPattern.replaceAll( "/", REGEX_SAFE_SLASH );
				theRegex = theRegex.replaceAll( "\\.", REGEX_SAFE_DOT );
				theRegex = theRegex.replaceAll( "(?<!\\*)\\*(?!\\*)", WILDCARD_ASTERISK );
				theRegex = theRegex.replaceAll( "\\*\\*", WILDCARD_DOUBOLE_ASTERISK );
				theRegex = theRegex.replaceAll( "\\?", WILDCARD_QUESTION_MARK );
				// <-- Convert the wildcards into regex pattern groups
				// Convert the wildcard names into regex pattern group names -->
				String eTruncated, eName, eHead, eTail;
				String theAsterisk = WILDCARD_ASTERISK.replaceAll( "\\\\", "" );
				String theDoubleAsterisk = WILDCARD_DOUBOLE_ASTERISK.replaceAll( "(\\\\)\\\\", "$1" );
				String theQuestionMark = WILDCARD_QUESTION_MARK.replaceAll( "(\\\\)\\\\", "$1" );
				List<String> theWildcardNames = new ArrayList<>();
			*/
			// @formatter:on
		// <--| Previously

		final String WILDCARD_NAME_PATTERN = "(?<=\\$\\{).*?(?=\\}\\=)"; // ${name}=
		final int WILDCARD_NAME_PREFIX_LENGTH = 2; // "${"
		final int WILDCARD_NAME_SUFFIX_LENGTH = 2; // "}="
		final String SIMPLE_NAME_PATTERN = "(?<=\\$\\{).*?(?=\\})"; // ${name}
		final int SIMPLE_NAME_PREFIX_LENGTH = 2; // "${"
		final int SIMPLE_NAME_SUFFIX_LENGTH = 1; // "}"

		String theDelimiter = toEscapedInsiceCharacterClass( aDelimiter );

		if ( aDelimiter == Delimiter.DOS_PATH.getChar() ) {
			theDelimiter += theDelimiter;
		}

		final String WILDCARD_ASTERISK = "([^" + theDelimiter + "]*)";

		final String WILDCARD_DOUBOLE_ASTERISK = "(.*)";
		final String REGEX_DELIMITER = toEscapedOutsiceCharacterClass( aDelimiter );
		String REGEX_SAFE_DELIMITER = REGEX_DELIMITER;
		if ( aDelimiter == Delimiter.DOS_PATH.getChar() ) {
			REGEX_SAFE_DELIMITER += REGEX_SAFE_DELIMITER;
		}
		final String REGEX_SAFE_DOT = "\\.";

		// Convert the wildcards into regex pattern groups -->
		String theRegex = aPathPattern;
		theRegex = aPathPattern.replaceAll( REGEX_DELIMITER, REGEX_SAFE_DELIMITER );

		theRegex = theRegex.replaceAll( "\\.", REGEX_SAFE_DOT );
		theRegex = theRegex.replaceAll( "(?<!\\*)\\*(?!\\*)", WILDCARD_ASTERISK );
		theRegex = theRegex.replaceAll( "\\*\\*", WILDCARD_DOUBOLE_ASTERISK );

		String WILDCARD_QUESTION_MARK;

		int theMaxQuestionMarks = 0;
		for ( int i = 0; i < aPathPattern.length(); i++ ) {
			if ( aPathPattern.charAt( i ) == '?' ) {
				theMaxQuestionMarks++;
			}
		}
		for ( int i = theMaxQuestionMarks; i > 0; i-- ) {
			WILDCARD_QUESTION_MARK = "(";
			String QUESTION_MARK = "";
			for ( int j = 0; j < i; j++ ) {
				WILDCARD_QUESTION_MARK += "\\\\w";
				QUESTION_MARK += "\\?";
			}
			WILDCARD_QUESTION_MARK += ")";
			theRegex = theRegex.replaceAll( QUESTION_MARK, WILDCARD_QUESTION_MARK );
		}
		WILDCARD_QUESTION_MARK = "(\\\\w";
		final String theQuestionMark = WILDCARD_QUESTION_MARK.replaceAll( "(\\\\)\\\\", "$1" );
		// theRegex = theRegex.replaceAll( "\\?", WILDCARD_QUESTION_MARK );
		// <-- Convert the wildcards into regex pattern groups

		// Convert the wildcard names into regex pattern group names -->

		String eTruncated;
		String eName;
		String eHead;
		String eTail;
		String theAsterisk = WILDCARD_ASTERISK.replaceAll( "\\\\", "" );
		if ( aDelimiter == Delimiter.DOS_PATH.getChar() ) {
			theAsterisk = WILDCARD_ASTERISK.replaceAll( "\\\\\\\\", "\\\\" );
		}

		final String theDoubleAsterisk = WILDCARD_DOUBOLE_ASTERISK.replaceAll( "(\\\\)\\\\", "$1" );
		final List<String> theWildcardNames = new ArrayList<>();
		// _formatter:off
		// <--| After

		// Named wildcards -->
		Pattern theJokerPattern = Pattern.compile( WILDCARD_NAME_PATTERN );
		java.util.regex.Matcher eJokerMatcher = theJokerPattern.matcher( theRegex );
		while ( eJokerMatcher.find() ) {
			eTruncated = theRegex.substring( eJokerMatcher.end() + WILDCARD_NAME_SUFFIX_LENGTH );
			if ( !eTruncated.startsWith( theAsterisk ) && !eTruncated.startsWith( theDoubleAsterisk ) && !eTruncated.startsWith( theQuestionMark ) ) {
				throw new IllegalArgumentException( "Your wildcard name (such as \"" + theRegex.substring( eJokerMatcher.start(), eJokerMatcher.start() ) + "\") in pattern <" + aPathPattern + "> must prefix a wildcard such as \"" + theAsterisk + "\" or \"" + theDoubleAsterisk + "\" or \"" + theQuestionMark + "!" );
			}
			eName = theRegex.substring( eJokerMatcher.start(), eJokerMatcher.end() );
			theWildcardNames.add( eName );
			eHead = theRegex.substring( 0, eJokerMatcher.start() - WILDCARD_NAME_PREFIX_LENGTH );
			eTail = theRegex.substring( eJokerMatcher.end() + WILDCARD_NAME_SUFFIX_LENGTH );
			eTail = eTail.substring( 0, 1 ) + "?<" + eName + ">" + eTail.substring( 1 );
			theRegex = eHead + eTail;
			eJokerMatcher = theJokerPattern.matcher( theRegex );
		}
		// <-- Named wildcards 

		// Simple name -->
		theJokerPattern = Pattern.compile( SIMPLE_NAME_PATTERN );
		eJokerMatcher = theJokerPattern.matcher( theRegex );
		while ( eJokerMatcher.find() ) {
			eName = theRegex.substring( eJokerMatcher.start(), eJokerMatcher.end() );
			theWildcardNames.add( eName );
			eHead = theRegex.substring( 0, eJokerMatcher.start() - SIMPLE_NAME_PREFIX_LENGTH );
			eTail = WILDCARD_ASTERISK + theRegex.substring( eJokerMatcher.end() + SIMPLE_NAME_SUFFIX_LENGTH );
			eTail = eTail.substring( 0, 1 ) + "?<" + eName + ">" + eTail.substring( 1 );
			theRegex = eHead + eTail;
			eJokerMatcher = theJokerPattern.matcher( theRegex );
		}
		// <-- Simple name

		if ( theWildcardNames.size() != 0 ) {
			_wildcardNames = theWildcardNames.toArray( new String[theWildcardNames.size()] );
		}
		// <-- Convert the wildcard names into regex pattern group names
		return theRegex;
	}

	private String toEscapedReservedChars( String aPathPattern ) {
		aPathPattern = toEscapeDollar( aPathPattern );
		aPathPattern = toEscapeOpenCurly( aPathPattern );
		return aPathPattern;

	}

	private String toEscapeDollar( String aPathPattern ) {
		String tmp;
		int tmpIndex;
		int index = aPathPattern.indexOf( DOLLAR );
		while ( index != -1 && ( aPathPattern.length() == 1 || index == aPathPattern.length() - 1 || ( index < aPathPattern.length() - 1 && aPathPattern.charAt( index + 1 ) != OPEN_CURLY ) ) ) {
			if ( index == 0 ) {
				aPathPattern = toEscapedOutsiceCharacterClass( DOLLAR ) + aPathPattern.substring( index + 1 );
				index++;
			}
			else {
				aPathPattern = aPathPattern.substring( 0, index ) + toEscapedOutsiceCharacterClass( DOLLAR ) + aPathPattern.substring( index + 1 );
				index++;
			}
			tmp = aPathPattern.substring( index + 1 );
			tmpIndex = index;
			index = tmp.indexOf( DOLLAR );
			if ( index != -1 ) {
				index += tmpIndex + 1;
			}
		}
		return aPathPattern;
	}

	private String toEscapeOpenCurly( String aPathPattern ) {
		String tmp;
		int tmpIndex;
		int index = aPathPattern.indexOf( OPEN_CURLY );
		while ( index != -1 && index == 0 || ( index > 0 && aPathPattern.charAt( index - 1 ) != DOLLAR ) ) {
			if ( index == 0 ) {
				aPathPattern = toEscapedOutsiceCharacterClass( OPEN_CURLY ) + aPathPattern.substring( 1 );
				index++;
			}
			else {
				aPathPattern = aPathPattern.substring( 0, index ) + toEscapedOutsiceCharacterClass( OPEN_CURLY ) + aPathPattern.substring( index + 1 );
				index++;
			}
			tmp = aPathPattern.substring( index + 1 );
			tmpIndex = index;
			index = tmp.indexOf( OPEN_CURLY );
			if ( index != -1 ) {
				index += tmpIndex + 1;
			}
		}
		return aPathPattern;
	}

	private String toEscapedInsiceCharacterClass( char aChar ) {
		for ( char aESCAPE_INSIDE_CHARACTER_CLASS : ESCAPE_INSIDE_CHARACTER_CLASS ) {
			if ( aESCAPE_INSIDE_CHARACTER_CLASS == aChar ) {
				return "\\" + aChar;
			}
		}
		return "" + aChar;
	}

	private String toEscapedOutsiceCharacterClass( char aChar ) {
		for ( char aESCAPE_OUTSIDE_CHARACTER_CLASS : ESCAPE_OUTSIDE_CHARACTER_CLASS ) {
			if ( aESCAPE_OUTSIDE_CHARACTER_CLASS == aChar ) {
				return "\\" + aChar;
			}
		}
		return "" + aChar;
	}
}