// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.matcher;

/**
 * A logical OR implementation of a {@link Matcher} encapsulating
 * {@link Matcher} instances. The OR {@link Matcher} matches when any of the
 * provided {@link Matcher} instances does match (OR).
 *
 * @param <M> The matchee type
 */
public class OrMatcher<M> extends AbstractMatcherComposite<M> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ALIAS = "OR";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an OR {@link Matcher}, i.e. any of the provided
	 * {@link Matcher} instances need to match in order for the OR
	 * {@link Matcher} to match.
	 * 
	 * @param aMatchers The {@link Matcher} instances to be combined with a
	 *        logical OR.
	 */
	@SafeVarargs
	public OrMatcher( Matcher<M>... aMatchers ) {
		super( ALIAS, "At least one nested matcher must match (OR).", aMatchers );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isMatching( M aMatchee ) {
		assert ( aMatchee != null );
		for ( Matcher<M> _matcher : _matchers ) {
			if ( _matcher.isMatching( aMatchee ) ) {
				return true;
			}
		}
		return false;
	}
}