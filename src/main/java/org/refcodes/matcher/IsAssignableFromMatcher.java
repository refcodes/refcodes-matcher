// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.matcher;

import org.refcodes.schema.Schema;

/**
 * A IS ASSIGNABLE FROM implementation of a {@link Matcher} by type.
 *
 * @param <M> The matchee type
 */
public class IsAssignableFromMatcher<M> extends AbstractMatcher<M> {

	public static final String ALIAS = "IS_ASSIGNABLE_FROM";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Class<?> _matcheeType;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a IS ASSIGNABLE FROM {@link Matcher} using the type of the
	 * matchees being the criteria to determine a match.
	 * 
	 * @param aMatcheeType The type of the matchee to match.
	 */
	public IsAssignableFromMatcher( Class<?> aMatcheeType ) {
		super( ALIAS, "Tests the matchees for assignable from (IS ASSIGNABLE FROM)." );
		_matcheeType = aMatcheeType;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isMatching( M aMatchee ) {
		assert ( aMatchee != null );
		if ( _matcheeType != null ) {
			if ( !_matcheeType.isAssignableFrom( aMatchee.getClass() ) ) {
				return false;
			}
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MatcherSchema toSchema() {
		final MatcherSchema theSchema = super.toSchema();
		theSchema.put( Schema.VALUE, _matcheeType );
		return theSchema;
	}
}