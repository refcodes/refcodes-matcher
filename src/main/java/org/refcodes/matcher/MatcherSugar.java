// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.matcher;

/**
 * Declarative syntactic sugar which may be statically imported in order to
 * allow declarative definitions for the {@link Matcher} elements.
 */
public class MatcherSugar {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Instantiates a new matcher sugar.
	 */
	protected MatcherSugar() {}

	// /////////////////////////////////////////////////////////////////////////
	// IMPORT STATICS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Static import IS ASSIGNABLE FROM {@link Matcher} using the type of the
	 * matchees being the criteria to determine a match.
	 * 
	 * @param <M> The type of the matchee to be matched
	 * @param aMatcheeType The type of the matchee to match.
	 * 
	 * @return A {@link Matcher} by type.
	 */
	public static <M> Matcher<M> isAssignableFrom( Class<?> aMatcheeType ) {
		return new IsAssignableFromMatcher<>( aMatcheeType );
	}

	/**
	 * Import static OR {@link Matcher}, i.e. any of the provided
	 * {@link Matcher} instances need to match in order for the OR
	 * {@link Matcher} to match.
	 * 
	 * @param <M> The type of the matchee to be matched
	 * @param aMatchers The {@link Matcher} instances to be combined with a
	 *        logical OR.
	 * 
	 * @return An OR {@link Matcher}.
	 */
	@SafeVarargs
	public static <M> Matcher<M> or( Matcher<M>... aMatchers ) {
		return new OrMatcher<>( aMatchers );
	}

	/**
	 * Import static AND {@link Matcher}, i.e. all of the provided
	 * {@link Matcher} instances need to match in order for the AND
	 * {@link Matcher} to match.
	 * 
	 * @param <M> The type of the matchee to be matched
	 * @param aMatchers The {@link Matcher} instances to be combined with a
	 *        logical AND.
	 * 
	 * @return An AND {@link Matcher}.
	 */
	@SafeVarargs
	public static <M> Matcher<M> and( Matcher<M>... aMatchers ) {
		return new AndMatcher<>( aMatchers );
	}

	/**
	 * Import static NOT {@link Matcher} whose
	 * {@link Matcher#isMatching(Object)} method inverts (NOT) the
	 * {@link Matcher#isMatching(Object)} result of the encapsulated
	 * {@link Matcher}.
	 * 
	 * @param <M> The type of the matchee to be matched
	 * @param aMatcher The {@link Matcher} to be wrapped with a NOT.
	 * 
	 * @return A NOT {@link Matcher}.
	 */
	public static <M> Matcher<M> not( Matcher<M> aMatcher ) {
		return new NotMatcher<>( aMatcher );
	}

	/**
	 * Import static ANY implementation of a {@link Matcher} which always
	 * returns <code>true</code>.
	 * 
	 * @param <M> The matchee type.
	 * 
	 * @return An ANY {@link Matcher}.
	 */
	public static <M> Matcher<M> any() {
		return new AnyMatcher<>();
	}

	/**
	 * Import static NONE implementation of a {@link Matcher} which always
	 * returns <code>false</code>.
	 * 
	 * @param <M> The matchee type.
	 * 
	 * @return A NONE {@link Matcher}.
	 */
	public static <M> Matcher<M> none() {
		return new NoneMatcher<>();
	}

	/**
	 * Import static EQUAL WITH {@link Matcher}, comparing its matchee with the
	 * matchee provided to the {@link Matcher#isMatching(Object)} method for
	 * equality.
	 * 
	 * @param <M> The matchee type.
	 * @param aMatchee The matchee with which to match.
	 * 
	 * @return An EQUAL WITH {@link Matcher}.
	 */
	public static <M> Matcher<M> equalWith( M aMatchee ) {
		return new EqualWithMatcher<>( aMatchee );
	}

	/**
	 * Import static NOT EQUAL WITH {@link Matcher}, comparing its matchee with
	 * the matchee provided to the {@link Matcher#isMatching(Object)} method for
	 * equality.
	 * 
	 * @param <M> The matchee type.
	 * @param aMatchee The matchee with which to match.
	 * 
	 * @return An NOT EQUAL WITH {@link Matcher}.
	 */
	public static <M> Matcher<M> notEqualWith( M aMatchee ) {
		return new NotEqualWithMatcher<>( aMatchee );
	}

	/**
	 * Import static GREATER OR EQUAL THAN {@link Matcher}, comparing its
	 * matchee with the matchee provided to the
	 * {@link Matcher#isMatching(Object)} method using the
	 * {@link Comparable#compareTo(Object)} method to be implemented by the
	 * matchees.
	 * 
	 * @param <M> The matchee type.
	 * @param aMatchee The matchee with which to match.
	 * 
	 * @return A NOT EQUAL WITH {@link Matcher}.
	 */
	public static <M extends Comparable<M>> Matcher<M> greaterOrEqualThan( M aMatchee ) {
		return new GreaterOrEqualThanMatcher<>( aMatchee );
	}

	/**
	 * Import static GREATER THAN {@link Matcher}, comparing its matchee with
	 * the matchee provided to the {@link Matcher#isMatching(Object)} method
	 * using the {@link Comparable#compareTo(Object)} method to be implemented
	 * by the matchees.
	 * 
	 * @param <M> The matchee type.
	 * @param aMatchee The matchee with which to match.
	 * 
	 * @return A GREATER THAN {@link Matcher}.
	 */
	public static <M extends Comparable<M>> Matcher<M> greaterThan( M aMatchee ) {
		return new GreaterThanMatcher<>( aMatchee );
	}

	/**
	 * Import static LESS OR EQUAL THAN {@link Matcher}, comparing its matchee
	 * with the matchee provided to the {@link Matcher#isMatching(Object)}
	 * method using the {@link Comparable#compareTo(Object)} method to be
	 * implemented by the matchees.
	 * 
	 * @param <M> The matchee type.
	 * @param aMatchee The matchee with which to match.
	 * 
	 * @return An LESS OR EQUAL THAN {@link Matcher}.
	 */
	public static <M extends Comparable<M>> Matcher<M> lessOrEqualThan( M aMatchee ) {
		return new LessOrEqualThanMatcher<>( aMatchee );
	}

	/**
	 * Import static LESS THAN {@link Matcher}, comparing its matchee with the
	 * matchee provided to the {@link Matcher#isMatching(Object)} method using
	 * the {@link Comparable#compareTo(Object)} method to be implemented by the
	 * matchees.
	 * 
	 * @param <M> The matchee type.
	 * @param aMatchee The matchee with which to match.
	 * 
	 * @return An LESS THAN {@link Matcher}.
	 */
	public static <M extends Comparable<M>> Matcher<M> lessThan( M aMatchee ) {
		return new LessThanMatcher<>( aMatchee );
	}
}
