// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.matcher;

/**
 * An EQUAL WITH {@link Matcher}, comparing its matchee with the matchee
 * provided to the {@link #isMatching(Object)} method for equality.
 *
 * @param <M> The matchee type.
 */
public class EqualWithMatcher<M> extends AbstractMatcheeMatcher<M> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ALIAS = "EQUAL_WITH";

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs an EQUAL WITH {@link Matcher}, comparing its matchee with the
	 * matchee provided to the {@link #isMatching(Object)} method for equality.
	 * 
	 * @param aMatchee The matchee with which to match.
	 */
	public EqualWithMatcher( M aMatchee ) {
		super( ALIAS, "Compares the matchees for equality (EQUAL WITH).", aMatchee );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isMatching( M aMatchee ) {
		return _matchee.equals( aMatchee );
	}
}