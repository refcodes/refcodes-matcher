// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.matcher;

/**
 * A logical NOT implementation of a {@link Matcher} encapsulating another
 * {@link Matcher}. The NOT {@link Matcher}'s {@link #isMatching(Object)} method
 * inverts (NOT) the {@link #isMatching(Object)} result of the encapsulated
 * {@link Matcher}.
 *
 * @param <M> The matchee type.
 */
public class NotMatcher<M> extends AbstractMatcher<M> {

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	public static final String ALIAS = "NOT";

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private final Matcher<M> _matcher;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a NOT {@link Matcher} whose {@link #isMatching(Object)} method
	 * inverts (NOT) the {@link #isMatching(Object)} result of the encapsulated
	 * {@link Matcher}.
	 * 
	 * @param aMatcher The {@link Matcher} to be wrapped with a NOT.
	 */
	public NotMatcher( Matcher<M> aMatcher ) {
		super( ALIAS, "Inverts the comparison of the wrapped matcher (NOT)." );
		_matcher = aMatcher;
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isMatching( M aMatchee ) {
		assert ( aMatchee != null );
		return !_matcher.isMatching( aMatchee );
	}
}