// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// /////////////////////////////////////////////////////////////////////////////
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// -----------------------------------------------------------------------------
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// -----------------------------------------------------------------------------
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// -----------------------------------------------------------------------------
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.matcher;

import org.refcodes.mixin.WildcardSubstitutes;

/**
 * A {@link WildcardMatcher} is a {@link Matcher} providing additional means to
 * work with the wildcards and their wildcard substitutes in case of a match as
 * of {@link #isMatching(Object)}.
 */
public interface WildcardMatcher extends Matcher<String> {

	/**
	 * Returns all available wildcard substitutes as well as the named wildcard
	 * substitutes.
	 * 
	 * @param aText Tex textfor which to retrieve the wildcard substitutes.
	 * 
	 * @return The {@link WildcardSubstitutes} of the wildcards being
	 *         substituted.
	 */
	WildcardSubstitutes toWildcardSubstitutes( String aText );

	/**
	 * Returns an array of the wildcard substitutes for the wildcards in your
	 * pattern compared to the actual text (as of
	 * {@link WildcardMatcher#toWildcardSubstitutes(String)}). The order of the
	 * wildcard substitutes aligns to the order of the wildcards (from left to
	 * right) defined in your pattern.
	 * 
	 * @param aText Tex textfor which to retrieve the wildcard substitutes.
	 * 
	 * @return The text substituting the wildcards in the order of the wildcards
	 *         being substituted or null if there are none such substitutes.
	 */
	String[] toWildcardReplacements( String aText );

	/**
	 * Returns the wildcard substitute for the wildcards in your pattern
	 * compared to the actual text (as of
	 * {@link WildcardMatcher#toWildcardSubstitutes(String)}). The text of the
	 * wildcard substitute aligns to the index of the wildcard (from left to
	 * right) as defined in your pattern.
	 * 
	 * @param aText Tex textfor which to retrieve the wildcard substitutes.
	 * @param aIndex The index of the wildcard in question for which to retrieve
	 *        the substitute.
	 * 
	 * @return The text substituting the wildcard at the given pattern's
	 *         wildcard index or null if there is none such substitute.
	 */
	String toWildcardReplacementAt( String aText, int aIndex );

	/**
	 * Returns the wildcard substitutes for the wildcards in your pattern
	 * compared to the actual text. The text of the wildcard substitutes aligns
	 * to the indexes of the wildcard (from left to right) as defined in your
	 * pattern.
	 * 
	 * @param aText Tex textfor which to retrieve the wildcard substitutes.
	 * @param aIndexes The indexes of the wildcards in question for which to
	 *        retrieve the substitutes.
	 * 
	 * @return The text substituting the wildcards at the given pattern's
	 *         wildcard indexes or null if there is none such substitute.
	 */
	String[] toWildcardReplacementsAt( String aText, int... aIndexes );

	/**
	 * Returns the wildcard substitute for the wildcards in your pattern
	 * compared to the actual text. The text of the wildcard substitute aligns
	 * to the name of the wildcard (as defined in your pattern).
	 * 
	 * @param aText Tex textfor which to retrieve the wildcard substitutes.
	 * @param aWildcardName The name of the wildcard in question for which to
	 *        retrieve the substitute.
	 * 
	 * @return The text substituting the wildcard with the given pattern's
	 *         wildcard name or null if there is none such substitute.
	 */
	String toWildcardReplacement( String aText, String aWildcardName );

	/**
	 * Returns the wildcard substitutes for the wildcards in your pattern
	 * compared to the actual text . The text of the wildcard substitutes aligns
	 * to the order of the provided wildcard names (as defined in your pattern).
	 * 
	 * @param aText Tex textfor which to retrieve the wildcard substitutes.
	 * @param aWildcardNames The names of the wildcards in question for which to
	 *        retrieve the substitutes in the order of the provided names.
	 * 
	 * @return The text substituting the wildcard with the given pattern's
	 *         wildcard names or null if there are none such substitute.
	 */
	String[] toWildcardReplacements( String aText, String... aWildcardNames );

	/**
	 * Retrieves the list of wildcard names identifying the wildcards as
	 * specified by the pattern.
	 * 
	 * @return The wild card names or null of no wild card names have been
	 *         defined.
	 */
	String[] getWildcardNames();

}