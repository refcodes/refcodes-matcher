// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.matcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.refcodes.mixin.PatternAccessor;
import org.refcodes.mixin.WildcardSubstitutes;

/**
 * The REGEXP {@link RegExpMatcher} provides {@link WildcardMatcher}
 * functionality for regular expression {@link Pattern} matching.
 */
public class RegExpMatcher extends AbstractWildcardMatcher<String> implements WildcardMatcher, PatternAccessor {

	// /////////////////////////////////////////////////////////////////////////
	// VARIABLES:
	// /////////////////////////////////////////////////////////////////////////

	private Pattern _matchee;
	private String[] _wildcardNames = null;

	// /////////////////////////////////////////////////////////////////////////
	// CONSTRUCTORS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * Constructs a {@link Pattern} matcher.
	 * 
	 * @param aRegExp The pattern to be used when matching a path via
	 *        {@link #isMatching(String)}.
	 */
	public RegExpMatcher( Pattern aRegExp ) {
		super( "Matches a text against a regular expression (REGEXP)." );
		_matchee = aRegExp;
		final List<String> theGroups = new ArrayList<>();
		final Matcher theMatcher = Pattern.compile( "\\(\\?<.*?>.*?\\)" ).matcher( _matchee.pattern() ); // <[^>]*>
		String eGroup;
		int begin;
		int end;
		while ( theMatcher.find() ) {
			eGroup = theMatcher.group();
			begin = eGroup.indexOf( '<' ) + 1;
			end = eGroup.indexOf( '>' );
			eGroup = eGroup.substring( begin, end );
			theGroups.add( eGroup );
		}
		_wildcardNames = theGroups.toArray( new String[theGroups.size()] );
	}

	/**
	 * Constructs a {@link Pattern} matcher.
	 * 
	 * @param aRegExp The pattern to be used when matching a path via
	 *        {@link #isMatching(String)}.
	 * 
	 * @throws PatternSyntaxException Thrown if the expression's syntax is
	 *         invalid
	 */
	public RegExpMatcher( String aRegExp ) {
		this( Pattern.compile( aRegExp ) );
	}

	// /////////////////////////////////////////////////////////////////////////
	// METHODS:
	// /////////////////////////////////////////////////////////////////////////

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] getWildcardNames() {
		return _wildcardNames;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Pattern getPattern() {
		return _matchee;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean isMatching( String aText ) {
		final Matcher theMatcher = _matchee.matcher( aText );
		return theMatcher.matches();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public WildcardSubstitutes toWildcardSubstitutes( String aText ) {
		final java.util.regex.Matcher theMatcher = _matchee.matcher( aText );
		if ( !theMatcher.matches() ) {
			return null;
		}
		// Unnamed substitutes -->
		final List<String> theWildcardSubstitutes = new ArrayList<>();
		for ( int i = 0; i < theMatcher.groupCount(); i++ ) {
			theWildcardSubstitutes.add( theMatcher.group( i + 1 ) );
		}
		final String[] theWildcardReplacements = theWildcardSubstitutes.toArray( new String[theWildcardSubstitutes.size()] );
		// <-- Unnamed substitutes

		// Named substitutes -->
		Map<String, String> theNamedWildcardSubstitutes = null;
		if ( _wildcardNames != null ) {
			theNamedWildcardSubstitutes = new HashMap<>();
			for ( String eName : _wildcardNames ) {
				theNamedWildcardSubstitutes.put( eName, theMatcher.group( eName ) );
			}
		}
		// <-- Named substitutes

		return new WildcardMatcherSubstitutes( theWildcardReplacements, theNamedWildcardSubstitutes );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toWildcardReplacements( String aText ) {
		final java.util.regex.Matcher theMatcher = _matchee.matcher( aText );
		if ( !theMatcher.matches() ) {
			return null;
		}
		final List<String> theWildcardSubstitutes = new ArrayList<>();
		for ( int i = 0; i < theMatcher.groupCount(); i++ ) {
			theWildcardSubstitutes.add( theMatcher.group( i + 1 ) );
		}
		return theWildcardSubstitutes.toArray( new String[theWildcardSubstitutes.size()] );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toWildcardReplacementAt( String aText, int aIndex ) {
		final java.util.regex.Matcher theMatcher = _matchee.matcher( aText );
		if ( !theMatcher.matches() ) {
			return null;
		}
		if ( aIndex < theMatcher.groupCount() ) {
			return theMatcher.group( aIndex + 1 );
		}
		throw new IllegalArgumentException( "Your provided index <" + aIndex + "> exceeds the number if wildcards <" + theMatcher.groupCount() + "> defined in your pattern \"" + getPattern().pattern() + "\" for current path \"" + aText + "\"." );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toWildcardReplacementsAt( String aText, int... aIndexes ) {
		final java.util.regex.Matcher theMatcher = _matchee.matcher( aText );
		if ( !theMatcher.matches() ) {
			return null;
		}
		if ( aIndexes == null || aIndexes.length == 0 ) {
			return new String[] {};
		}
		final List<String> theWildcardSubstitutes = new ArrayList<>();
		for ( int i : aIndexes ) {
			if ( i >= theMatcher.groupCount() ) {
				throw new IllegalArgumentException( "Your provided index <" + i + "> exceeds the number of wildcards <" + theMatcher.groupCount() + "> defined in your pattern \"" + getPattern().pattern() + "\" for current path \"" + aText + "\"." );
			}
			theWildcardSubstitutes.add( theMatcher.group( i + 1 ) );
		}
		return theWildcardSubstitutes.toArray( new String[theWildcardSubstitutes.size()] );
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String toWildcardReplacement( String aText, String aWildcardName ) {
		final java.util.regex.Matcher theMatcher = _matchee.matcher( aText );
		if ( !theMatcher.matches() ) {
			return null;
		}
		try {
			return theMatcher.group( aWildcardName );
		}
		catch ( IllegalArgumentException e ) {
			throw new IllegalArgumentException( "Your provided name <" + aWildcardName + "> is not defined in your pattern \"" + getPattern().pattern() + "\" for current path \"" + aText + "\".", e );
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String[] toWildcardReplacements( String aText, String... aWildcardNames ) {
		final java.util.regex.Matcher theMatcher = _matchee.matcher( aText );
		if ( !theMatcher.matches() ) {
			return null;
		}
		if ( aWildcardNames == null || aWildcardNames.length == 0 ) {
			return new String[] {};
		}
		final List<String> theWildcardSubstitutes = new ArrayList<>();
		for ( String eWildcardName : aWildcardNames ) {
			try {
				theWildcardSubstitutes.add( theMatcher.group( eWildcardName ) );
			}
			catch ( IllegalArgumentException e ) {
				throw new IllegalArgumentException( "Your provided name <" + eWildcardName + "> is not defined in your pattern \"" + getPattern().pattern() + "\" for current path \"" + aText + "\".", e );
			}
		}
		return theWildcardSubstitutes.toArray( new String[theWildcardSubstitutes.size()] );
	}

	/**
	 * For debugging purposes, retrieve the regex pattern created from the ANT
	 * path pattern.
	 * 
	 * @return Returns the regex created from the ANT path pattern.
	 */
	public String toPattern() {
		return _matchee.pattern();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ( ( _matchee == null ) ? 0 : _matchee.hashCode() );
		return result;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean equals( Object obj ) {
		if ( this == obj ) {
			return true;
		}
		if ( obj == null ) {
			return false;
		}
		if ( getClass() != obj.getClass() ) {
			return false;
		}
		final RegExpMatcher other = (RegExpMatcher) obj;
		if ( _matchee == null ) {
			if ( other._matchee != null ) {
				return false;
			}
		}
		else if ( !_matchee.equals( other._matchee ) ) {
			return false;
		}
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public MatcherSchema toSchema() {
		final MatcherSchema theSchema = super.toSchema();
		theSchema.put( REGEX_PATTERN, _matchee );
		theSchema.put( WILDCARD_NAMES, _wildcardNames );
		return theSchema;
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////
}