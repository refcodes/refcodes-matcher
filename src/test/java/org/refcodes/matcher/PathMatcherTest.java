// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.matcher;

import static org.junit.jupiter.api.Assertions.*;
import java.util.Arrays;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.refcodes.data.Delimiter;
import org.refcodes.mixin.WildcardSubstitutes;

public class PathMatcherTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String TEST_CASE = "/foo/acme/atari/bar";

	// @formatter:off
	private static final PatternMatchCase[] TEST_CASES = new PatternMatchCase[] {
		new PatternMatchCase("/foo/acme/atari/bar", true, null),
		new PatternMatchCase("/foo/acme/atari/b??", true, null),
		new PatternMatchCase("/foo/acme/atari/?bar", false, null),
		new PatternMatchCase("/foo/acme/atar?/?ar", true, null),
		new PatternMatchCase("/foo/acme/atari/*", true, null),
		new PatternMatchCase("/foo/acme/schneider/*", false, null),
		new PatternMatchCase("/foo/acme/**schneider/*", false, null),
		new PatternMatchCase("/foo/acme/**", true, null),
		new PatternMatchCase("/foo/acme/*/bar", true, null),
		new PatternMatchCase("/foo/*/atari/bar", true, null),
		new PatternMatchCase("/*/acme/atari/bar", true, null),
		new PatternMatchCase("**/foo/acme/atari/bar", true, null),
		new PatternMatchCase("**foo/acme/atari/bar", true, null),
		new PatternMatchCase("*/foo/acme/atari/bar", true, null),
		new PatternMatchCase("**/bar", true, null),
		new PatternMatchCase("/*/*/bar", false, null),
		new PatternMatchCase("**/*/*/bar", true, null),
		new PatternMatchCase("/**/*/*/bar", true, null),
		new PatternMatchCase("/*/*/*/bar", true, null),
		new PatternMatchCase("/foo/*/*/bar", true, null),
		new PatternMatchCase("/foo/*/bar", false, null),
		new PatternMatchCase("/foo/**/bar", true, null),
		new PatternMatchCase("/foo/*/**/bar", true, null),
		new PatternMatchCase("/foo/a*/*/bar", true, null),
		new PatternMatchCase("/foo/a*/**/bar", true, null),
		new PatternMatchCase("/foo/b*/*/bar", false, null),
		new PatternMatchCase("/foo/b*/**/bar", false, null),
		new PatternMatchCase("/foo/acme/atari/*", true, null),
		new PatternMatchCase("/foo/acme/atari/**", true, null),
		new PatternMatchCase("/foo/acme/atari*/*", true, null),
		new PatternMatchCase("/foo/acme/sinclair/*", false, null),
		new PatternMatchCase("/foo/**/bar", true, null),
		new PatternMatchCase("/foo/**/*b*ar", true, null),
		new PatternMatchCase("/foo/**/*b**ar", true, null),
		new PatternMatchCase("/foo/**/*c*ar", false, null),
		new PatternMatchCase("/foo/acme/atari/bar/*", false, null), // Do we need this to be true?
		new PatternMatchCase("/foo/acme/atari/bar/**", false, null),  // Do we need this to be true?
		new PatternMatchCase("/foo/${arg1}/atari/bar", true, new String[]{"arg1"}),
		new PatternMatchCase("/foo/${arg1}=*/atari/bar", true, new String[]{"arg1"}),
		new PatternMatchCase("/foo/acme/atari/b${arg1}=??", true, new String[]{"arg1" }),
		new PatternMatchCase("/foo/acme/atari/${arg1}=?bar", false, null),
		new PatternMatchCase("/foo/acme/atar${arg1}=?/${arg2}=?ar", true, new String[]{"arg1", "arg2"}),
		new PatternMatchCase("/foo/acme/atari/${arg1}=*", true, new String[]{"arg1"}),
		new PatternMatchCase("/foo/acme/schneider/${arg1}=*", false, null),
		new PatternMatchCase("/foo/acme/${arg1}=**schneider/${arg2}=*", false, null),
	};
	// @formatter:on

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testPathMatcher() {
		PathMatcher eMatcher;
		int i = 0;
		for ( PatternMatchCase eCase : TEST_CASES ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + i + "] " + eCase.isMatch() + ": Testing <" + TEST_CASE + "> against <" + eCase.getPattern() + "> to be <" + eCase.isMatch() + "> ..." );
			}
			eMatcher = new PathMatcher( eCase.getPattern() );
			assertEquals( eCase.isMatch(), eMatcher.isMatching( TEST_CASE ) );
			i++;
		}
	}

	@Test
	public void testPathMatcherWithDelimiter1() {
		PathMatcher eMatcher;
		int i = 0;
		final char theDelimiter = Delimiter.DOS_PATH.getChar();
		final String testCase = TEST_CASE.replace( Delimiter.PATH.getChar(), theDelimiter );
		for ( PatternMatchCase eCase : TEST_CASES ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + i + "] " + eCase.isMatch() + ": Testing <" + testCase + "> against <" + eCase.getPattern( theDelimiter ) + "> to be <" + eCase.isMatch() + "> ..." );
			}
			eMatcher = new PathMatcher( eCase.getPattern( theDelimiter ), theDelimiter );
			final String[] theResult = eMatcher.toWildcardReplacements( testCase );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + i + "] " + eCase.isMatch() + ": Diff between <" + TEST_CASE + "> and <" + eCase.getPattern() + "> for " + toVerbose( eCase.getWildcardNames() ) + " is := " + toVerbose( theResult ) );
			}
			assertEquals( eCase.isMatch(), eMatcher.isMatching( testCase ) );
			i++;
		}
	}

	@Test
	public void testPathMatcherWithDelimiter2() {
		PathMatcher eMatcher;
		int i = 0;
		final char theDelimiter = 'X';
		final String testCase = TEST_CASE.replace( Delimiter.PATH.getChar(), theDelimiter );
		for ( PatternMatchCase eCase : TEST_CASES ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + i + "] " + eCase.isMatch() + ": Testing <" + testCase + "> against <" + eCase.getPattern( theDelimiter ) + "> to be <" + eCase.isMatch() + "> ..." );
			}
			eMatcher = new PathMatcher( eCase.getPattern( theDelimiter ), theDelimiter );
			final String[] theResult = eMatcher.toWildcardReplacements( testCase );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + i + "] " + eCase.isMatch() + ": Diff between <" + TEST_CASE + "> and <" + eCase.getPattern() + "> for " + toVerbose( eCase.getWildcardNames() ) + " is := " + toVerbose( theResult ) );
			}
			assertEquals( eCase.isMatch(), eMatcher.isMatching( testCase ) );
			i++;
		}
	}

	@Test
	public void testPathMatcherWithDelimiter3() {
		PathMatcher eMatcher;
		int i = 0;
		final char theDelimiter = ':';
		final String testCase = TEST_CASE.replace( Delimiter.PATH.getChar(), theDelimiter );
		for ( PatternMatchCase eCase : TEST_CASES ) {
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + i + "] " + eCase.isMatch() + ": Testing <" + testCase + "> against <" + eCase.getPattern( theDelimiter ) + "> to be <" + eCase.isMatch() + "> ..." );
			}
			eMatcher = new PathMatcher( eCase.getPattern( theDelimiter ), theDelimiter );
			final String[] theResult = eMatcher.toWildcardReplacements( testCase );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + i + "] " + eCase.isMatch() + ": Diff between <" + TEST_CASE + "> and <" + eCase.getPattern() + "> for " + toVerbose( eCase.getWildcardNames() ) + " is := " + toVerbose( theResult ) );
			}
			assertEquals( eCase.isMatch(), eMatcher.isMatching( testCase ) );
			i++;
		}
	}

	@Test
	public void testWildcardSubstitutes() {
		PathMatcher eMatcher;
		int i = 0;
		for ( PatternMatchCase eCase : TEST_CASES ) {
			eMatcher = new PathMatcher( eCase.getPattern() );
			final String[] theResult = eMatcher.toWildcardReplacements( TEST_CASE );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + i + "] " + eCase.isMatch() + ": Diff between <" + TEST_CASE + "> and <" + eCase.getPattern() + "> for " + toVerbose( eCase.getWildcardNames() ) + " is := " + toVerbose( theResult ) );
			}
			assertEquals( eCase.isMatch(), theResult != null );
			i++;
		}
	}

	@Test
	public void testWildcardNameSubstitutes() {
		PathMatcher eMatcher;
		int i = 0;
		for ( PatternMatchCase eCase : TEST_CASES ) {
			eMatcher = new PathMatcher( eCase.getPattern() );
			final String[] theResult = eMatcher.toWildcardReplacements( TEST_CASE, eCase.getWildcardNames() );
			if ( IS_LOG_TESTS_ENABLED ) {
				System.out.println( "[" + i + "] " + eCase.isMatch() + ": Diff between <" + TEST_CASE + "> and <" + eCase.getPattern() + "> for " + toVerbose( eCase.getWildcardNames() ) + " is := " + toVerbose( theResult ) );
			}
			assertEquals( eCase.isMatch(), theResult != null );
			i++;
		}
	}

	@Test
	public void testEdgeCasesWildcardNames() {
		final String thePathPattern = "/foo/${arg1}=*/${arg2}=*/bar";
		final PathMatcher theMatcher = new PathMatcher( thePathPattern );
		final String[] theResult = theMatcher.toWildcardReplacements( TEST_CASE, "arg1", "arg2" );
		assertEquals( "acme", theResult[0] );
		assertEquals( "atari", theResult[1] );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( toVerbose( theResult ) );
		}
	}

	@Test
	public void testEdgeCasesSimpleNames() {
		final String thePathPattern = "/foo/${arg1}/atari/bar";
		final PathMatcher theMatcher = new PathMatcher( thePathPattern );
		final String[] theResult = theMatcher.toWildcardReplacements( TEST_CASE, "arg1" );
		assertEquals( "acme", theResult[0] );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( toVerbose( theResult ) );
		}
	}

	@Test
	public void testEdgeCase1() {
		final String thePathPattern = "/**/$ref";
		final PathMatcher theMatcher = new PathMatcher( thePathPattern );
		boolean isMatching = theMatcher.isMatching( "/foo/bar/hello/$ref" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertTrue( isMatching );
		isMatching = theMatcher.isMatching( "/foo/bar/hello/?ref" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertFalse( isMatching );
	}

	@Test
	public void testEdgeCase2() {
		final String thePathPattern = "/**/ref$";
		final PathMatcher theMatcher = new PathMatcher( thePathPattern );
		boolean isMatching = theMatcher.isMatching( "/foo/bar/hello/ref$" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertTrue( isMatching );
		isMatching = theMatcher.isMatching( "/foo/bar/hello/ref?" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertFalse( isMatching );
	}

	@Test
	public void testEdgeCase3() {
		final String thePathPattern = "$/**/ref";
		final PathMatcher theMatcher = new PathMatcher( thePathPattern );
		boolean isMatching = theMatcher.isMatching( "$/bar/hello/ref" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertTrue( isMatching );
		isMatching = theMatcher.isMatching( "?/bar/hello/ref" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertFalse( isMatching );
	}

	@Test
	public void testEdgeCase4() {
		final String thePathPattern = "{/**/ref";
		final PathMatcher theMatcher = new PathMatcher( thePathPattern );
		boolean isMatching = theMatcher.isMatching( "{/bar/hello/ref" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertTrue( isMatching );
		isMatching = theMatcher.isMatching( "?/bar/hello/ref" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertFalse( isMatching );
	}

	@Test
	public void testEdgeCase5() {
		final String thePathPattern = "/**/ref{";
		final PathMatcher theMatcher = new PathMatcher( thePathPattern );
		boolean isMatching = theMatcher.isMatching( "/bar/hello/ref{" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertTrue( isMatching );
		isMatching = theMatcher.isMatching( "/bar/hello/ref?" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertFalse( isMatching );
	}

	@Test
	public void testEdgeCase6() {
		final String thePathPattern = "/**/{ref";
		final PathMatcher theMatcher = new PathMatcher( thePathPattern );
		boolean isMatching = theMatcher.isMatching( "/bar/hello/{ref" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertTrue( isMatching );
		isMatching = theMatcher.isMatching( "/bar/hello/?ref" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertFalse( isMatching );
	}

	@Test
	public void testEdgeCase7() {
		final String thePathPattern = "{$/**/{$ref}$";
		final PathMatcher theMatcher = new PathMatcher( thePathPattern );
		boolean isMatching = theMatcher.isMatching( "{$/bar/hello/{$ref}$" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertTrue( isMatching );
		isMatching = theMatcher.isMatching( "{$/bar/hello/{$ref}?" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertFalse( isMatching );
	}

	@Test
	public void testEdgeCase8() {
		final String thePathPattern = "$/**/{$ref}$_{";
		final PathMatcher theMatcher = new PathMatcher( thePathPattern );
		boolean isMatching = theMatcher.isMatching( "$/bar/hello/{$ref}$_{" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertTrue( isMatching );
		isMatching = theMatcher.isMatching( "$/bar/hello/{$ref}?_{" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertFalse( isMatching );
	}

	@Test
	public void testEdgeCase9() {
		final String thePathPattern = "$";
		final PathMatcher theMatcher = new PathMatcher( thePathPattern );
		boolean isMatching = theMatcher.isMatching( "$" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertTrue( isMatching );
		isMatching = theMatcher.isMatching( "?" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertFalse( isMatching );
	}

	@Test
	public void testEdgeCase10() {
		final String thePathPattern = "{";
		final PathMatcher theMatcher = new PathMatcher( thePathPattern );
		boolean isMatching = theMatcher.isMatching( "{" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertTrue( isMatching );
		isMatching = theMatcher.isMatching( "?" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( "Is matching = " + isMatching );
		}
		assertFalse( isMatching );
	}

	@Test
	public void testToWildcardSubstitutes() {
		// String thePathPattern = "/*/${arg1}=*/${arg2}=*/${arg3}=*/${arg4}=A??";
		final String thePathPattern = "/*/${arg1}=*/${arg2}=*/${arg3}=*/A${arg4}=??";
		final PathMatcher theMatcher = new PathMatcher( thePathPattern );
		final String thePath = "/aaa/bbb/ccc/ddd/ABC";
		final WildcardSubstitutes theSubstitutes = theMatcher.toWildcardSubstitutes( thePath );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theSubstitutes );
		}
		final String[] theAliases = theSubstitutes.getWildcardNames();
		assertEquals( 4, theAliases.length );
		List<String> theList = Arrays.asList( theAliases );
		assertTrue( theList.contains( "arg1" ) );
		assertTrue( theList.contains( "arg2" ) );
		assertTrue( theList.contains( "arg3" ) );
		assertTrue( theList.contains( "arg4" ) );
		assertEquals( "bbb", theSubstitutes.getWildcardReplacement( "arg1" ) );
		assertEquals( "ccc", theSubstitutes.getWildcardReplacement( "arg2" ) );
		assertEquals( "ddd", theSubstitutes.getWildcardReplacement( "arg3" ) );
		assertEquals( "BC", theSubstitutes.getWildcardReplacement( "arg4" ) );
		theList = Arrays.asList( theSubstitutes.getWildcardReplacements() );
		for ( String eKey : theAliases ) {
			assertTrue( theList.contains( theSubstitutes.getWildcardReplacement( eKey ) ) );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * To verbose.
	 *
	 * @param aStrings the strings
	 * 
	 * @return the string
	 */
	private String toVerbose( String[] aStrings ) {
		if ( aStrings == null ) {
			return "{}";
		}
		String theVerbose = "{ ";
		for ( int l = 0; l < aStrings.length; l++ ) {
			theVerbose += "\"" + aStrings[l] + "\"";
			if ( l < aStrings.length - 1 ) {
				theVerbose += ", ";
			}
		}
		theVerbose += " }";
		return theVerbose;
	}

	// /////////////////////////////////////////////////////////////////////////
	// INNER CLASSES:
	// /////////////////////////////////////////////////////////////////////////
	/**
	 * The Class PatternMatchCase.
	 */
	private static class PatternMatchCase {
		private final String _pattern;
		private final boolean _isMatch;
		private final String[] _wildcardNames;

		/**
		 * Instantiates a new pattern match case.
		 *
		 * @param aPattern the pattern
		 * @param isMatch the is match
		 * @param aWildcardNames the wildcard names
		 */
		public PatternMatchCase( String aPattern, boolean isMatch, String[] aWildcardNames ) {
			_pattern = aPattern;
			_isMatch = isMatch;
			_wildcardNames = aWildcardNames;
		}

		/**
		 * Gets the pattern.
		 *
		 * @return the pattern
		 */
		public String getPattern() {
			return _pattern;
		}

		/**
		 * Gets the pattern.
		 *
		 * @return the pattern
		 */
		public String getPattern( char aDelimiter ) {
			return _pattern.replace( Delimiter.PATH.getChar(), aDelimiter );
		}

		/**
		 * Checks if is match.
		 *
		 * @return true, if is match
		 */
		public boolean isMatch() {
			return _isMatch;
		}

		/**
		 * Gets the wildcard names.
		 *
		 * @return the wildcard names
		 */
		public String[] getWildcardNames() {
			return _wildcardNames;
		}
	}
}
