// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.matcher;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.matcher.MatcherSugar.*;

import org.junit.jupiter.api.Test;

public class MatcherExamples {

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	@Test
	public void testMatcherSchema() {
		final Matcher<String> theSportsMatcher = or( equalWith( "Soccer" ), equalWith( "Football" ) );
		final Matcher<String> thePoliticsMatcher = or( equalWith( "War" ), equalWith( "Education" ) );
		final Matcher<String> theMediaMatcher = or( equalWith( "TV" ), equalWith( "Streaming" ) );
		final Matcher<String> theEntertainmentMatcher = or( theSportsMatcher, theMediaMatcher );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theSportsMatcher.toSchema() );
			System.out.println();
			System.out.println( thePoliticsMatcher.toSchema() );
			System.out.println();
			System.out.println( theMediaMatcher.toSchema() );
			System.out.println();
			System.out.println( theEntertainmentMatcher.toSchema() );
			System.out.println();
		}
	}

	@Test
	public void testPaths1() {
		final Matcher<String> theMatcher = new PathMatcher( "/*/acme/**" );
		String thePath = "/foo/acme/atari/bar";
		System.out.println( "\"" + thePath + "\" -?-> \"/*/acme/**\" --> " + theMatcher.isMatching( thePath ) );
		assertTrue( theMatcher.isMatching( thePath ) );
		thePath = "/bar/acme/commodore/bar";
		System.out.println( "\"" + thePath + "\" -?-> \"/*/acme/**\" --> " + theMatcher.isMatching( thePath ) );
		assertTrue( theMatcher.isMatching( thePath ) );
		thePath = "/foo/evil/bad/bar";
		System.out.println( "\"" + thePath + "\" -?-> \"/*/acme/**\" --> " + theMatcher.isMatching( thePath ) );
		assertFalse( theMatcher.isMatching( thePath ) );
	}

	@Test
	public void testPaths2() {
		final PathMatcher theMatcher = new PathMatcher( "/acme/**/${tail}" );
		String thePath = "/acme/foo/atari";
		String theVar = theMatcher.toWildcardReplacement( thePath, "tail" );
		System.out.println( "\"" + thePath + "\" -?-> \"/acme/**/${tail}\" --> " + theVar );
		assertEquals( "atari", theVar );
		thePath = "/acme/foo/commodore";
		theVar = theMatcher.toWildcardReplacement( thePath, "tail" );
		System.out.println( "\"" + thePath + "\" -?-> \"/acme/**/${tail}\" --> " + theVar );
		assertEquals( "commodore", theVar );
		thePath = "/acme/foo/what/ever/path/sinclair";
		theVar = theMatcher.toWildcardReplacement( thePath, "tail" );
		System.out.println( "\"" + thePath + "\" -?-> \"/acme/**/${tail}\" --> " + theVar );
		assertEquals( "sinclair", theVar );
		thePath = "/foo/what/ever/path/ghost";
		theVar = theMatcher.toWildcardReplacement( thePath, "tail" );
		System.out.println( "\"" + thePath + "\" -?-> \"/acme/**/${tail}\" --> " + theVar );
		assertNull( theVar );
	}

	@Test
	public void testInsideRange1() throws Exception {
		final Matcher<Integer> theMatcher = and( greaterOrEqualThan( -10 ), lessOrEqualThan( 10 ) ); // "-10 ≤ x ≤ 10
		for ( int i = -12; i <= 12; i++ ) {
			System.out.println( "-10 ≤ " + i + " ≤ 10 --> " + theMatcher.isMatching( i ) );
		}
		assertFalse( theMatcher.isMatching( -11 ) );
		assertTrue( theMatcher.isMatching( -10 ) );
		assertTrue( theMatcher.isMatching( -9 ) );
		assertTrue( theMatcher.isMatching( 0 ) );
		assertTrue( theMatcher.isMatching( 9 ) );
		assertTrue( theMatcher.isMatching( 10 ) );
		assertFalse( theMatcher.isMatching( 11 ) );
	}

	@Test
	public void testInsideRange2() throws Exception {
		final Matcher<Integer> theMatcher = and( greaterThan( -10 ), lessThan( 10 ) ); // -10 < x < 10
		for ( int i = -12; i < 12; i++ ) {
			System.out.println( "-10 < " + i + " < 10 --> " + theMatcher.isMatching( i ) );
		}
		assertFalse( theMatcher.isMatching( -11 ) );
		assertFalse( theMatcher.isMatching( -10 ) );
		assertTrue( theMatcher.isMatching( -9 ) );
		assertTrue( theMatcher.isMatching( 0 ) );
		assertTrue( theMatcher.isMatching( 9 ) );
		assertFalse( theMatcher.isMatching( 10 ) );
		assertFalse( theMatcher.isMatching( 11 ) );
	}

	@Test
	public void testInsideRange3() throws Exception {
		final Matcher<Integer> theMatcher = or( and( greaterOrEqualThan( -10 ), lessOrEqualThan( -5 ) ), and( greaterOrEqualThan( 5 ), lessOrEqualThan( 10 ) ) ); // -10 ≤ x ≤ -5 OR 5 ≤ x ≤ 10 
		for ( int i = -12; i <= 12; i++ ) {
			System.out.println( "-12 ≤ " + i + " ≤ 12 --> " + theMatcher.isMatching( i ) );
		}
		assertFalse( theMatcher.isMatching( -11 ) );
		assertTrue( theMatcher.isMatching( -10 ) );
		assertTrue( theMatcher.isMatching( -5 ) );
		assertFalse( theMatcher.isMatching( -4 ) );
		assertFalse( theMatcher.isMatching( 0 ) );
		assertFalse( theMatcher.isMatching( 4 ) );
		assertTrue( theMatcher.isMatching( 5 ) );
		assertTrue( theMatcher.isMatching( 10 ) );
		assertFalse( theMatcher.isMatching( 11 ) );
		System.out.println( theMatcher.toSchema() );

	}

	@Test
	public void testOutsideRange1() throws Exception {
		final Matcher<Integer> theMatcher = or( lessOrEqualThan( -10 ), greaterOrEqualThan( 10 ) ); // -10 ≰ x ≰ 10
		for ( int i = -12; i <= 12; i++ ) {
			System.out.println( "-10 ≰ " + i + " ≰ 10 --> " + theMatcher.isMatching( i ) );
		}
		assertTrue( theMatcher.isMatching( -11 ) );
		assertTrue( theMatcher.isMatching( -10 ) );
		assertFalse( theMatcher.isMatching( -9 ) );
		assertFalse( theMatcher.isMatching( 0 ) );
		assertFalse( theMatcher.isMatching( 9 ) );
		assertTrue( theMatcher.isMatching( 10 ) );
		assertTrue( theMatcher.isMatching( 11 ) );
	}

	@Test
	public void testOutsideRange2() throws Exception {
		final Matcher<Integer> theMatcher = or( lessThan( -10 ), greaterThan( 10 ) ); // -10 ≮ x ≮ 10
		for ( int i = -12; i <= 12; i++ ) {
			System.out.println( "-10 ≮ " + i + " ≮ 10 --> " + theMatcher.isMatching( i ) );
		}
		assertTrue( theMatcher.isMatching( -11 ) );
		assertFalse( theMatcher.isMatching( -10 ) );
		assertFalse( theMatcher.isMatching( -9 ) );
		assertFalse( theMatcher.isMatching( 0 ) );
		assertFalse( theMatcher.isMatching( 9 ) );
		assertFalse( theMatcher.isMatching( 10 ) );
		assertTrue( theMatcher.isMatching( 11 ) );
	}
}
