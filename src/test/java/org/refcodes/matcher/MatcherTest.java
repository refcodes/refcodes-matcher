// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.matcher;

import static org.junit.jupiter.api.Assertions.*;
import static org.refcodes.matcher.MatcherSugar.*;
import org.junit.jupiter.api.Test;

public class MatcherTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static final boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testIsType() throws Exception {
		final Matcher<Class1> theTypeMatcher = isAssignableFrom( Class1.class );
		assertTrue( theTypeMatcher.isMatching( new Class1() ) );
		assertTrue( theTypeMatcher.isMatching( new Class2() ) );
		assertTrue( theTypeMatcher.isMatching( new Class3() ) );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theTypeMatcher.toSchema() );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testEquals() throws Exception {
		final Matcher<String> theStringMatcher = equalWith( "Test" );
		assertTrue( theStringMatcher.isMatching( "Test" ) );
		assertFalse( theStringMatcher.isMatching( "Hallo" ) );
		assertFalse( theStringMatcher.isMatching( "Foo" ) );
		final Matcher<String> theNotMatcher = not( theStringMatcher );
		assertFalse( theNotMatcher.isMatching( "Test" ) );
		assertTrue( theNotMatcher.isMatching( "Hallo" ) );
		assertTrue( theNotMatcher.isMatching( "Foo" ) );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theStringMatcher.toSchema() );
		}
	}

	// /////////////////////////////////////////////////////////////////////////
	// HELPER:
	// /////////////////////////////////////////////////////////////////////////

	private class Class1 {}

	private class Class2 extends Class1 {}

	private class Class3 extends Class2 {}
}
