// /////////////////////////////////////////////////////////////////////////////
// REFCODES.ORG
// =============================================================================
// This code is copyright (c) by Siegfried Steiner, Munich, Germany, distributed
// on an "AS IS" BASIS WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, and licen-
// sed under the following (see "http://en.wikipedia.org/wiki/Multi-licensing")
// licenses:
// =============================================================================
// GNU General Public License, v3.0 ("http://www.gnu.org/licenses/gpl-3.0.html")
// together with the GPL linking exception applied; as being applied by the GNU
// Classpath ("http://www.gnu.org/software/classpath/license.html")
// =============================================================================
// Apache License, v2.0 ("http://www.apache.org/licenses/TEXT-2.0")
// =============================================================================
// Please contact the copyright holding author(s) of the software artifacts in
// question for licensing issues not being covered by the above listed licenses,
// also regarding commercial licensing models or regarding the compatibility
// with other open source licenses.
// /////////////////////////////////////////////////////////////////////////////

package org.refcodes.matcher;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.refcodes.mixin.WildcardSubstitutes;

public class RegExpMatcherTest {

	// /////////////////////////////////////////////////////////////////////////
	// STATICS:
	// /////////////////////////////////////////////////////////////////////////

	private static boolean IS_LOG_TESTS_ENABLED = Boolean.getBoolean( "log.tests" );

	// /////////////////////////////////////////////////////////////////////////
	// CONSTANTS:
	// /////////////////////////////////////////////////////////////////////////

	private static final String TEST_CASE = "/foo/acme/atari/bar";

	// /////////////////////////////////////////////////////////////////////////
	// TESTS:
	// /////////////////////////////////////////////////////////////////////////

	@Test
	public void testRegExpMatcher() {
		IS_LOG_TESTS_ENABLED = true;
		final RegExpMatcher theMatcher = new RegExpMatcher( "/foo/(?<arg1>[^/]*)/atari/bar" );
		final boolean isMatching = theMatcher.isMatching( TEST_CASE );
		assertTrue( isMatching );
		final WildcardSubstitutes theSubstitutes = theMatcher.toWildcardSubstitutes( TEST_CASE );
		final String[] theReplacements = theSubstitutes.getWildcardReplacements();
		if ( IS_LOG_TESTS_ENABLED ) {
			for ( String eReplacement : theReplacements ) {
				System.out.println( "Replacement = " + eReplacement );
			}
		}
		assertEquals( theReplacements.length, 1 );
		assertEquals( theReplacements[0], "acme" );
		final String theReplacement = theSubstitutes.getWildcardReplacement( "arg1" );
		if ( IS_LOG_TESTS_ENABLED ) {
			System.out.println( theReplacement );
		}
		assertEquals( "acme", theReplacement );
	}
}
